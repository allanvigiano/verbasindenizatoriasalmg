<?php

/**
 * Description of XmlController
 *
 * @author Allan
 */
set_time_limit(0); // evita que dê timeout na leitura do XML
require_once 'model/Deputado.php';
require_once 'model/banco/DeputadoDao.php';
require_once 'model/banco/DespesaDao.php';
require_once 'model/banco/TipoDespesaDao.php';
require_once 'model/Despesa.php';

class XmlController {
    static $deputadoAtual;
    static $numDeputados;
    public static function gravaListaDeputadosEmExercicio($url) {
        $xml = simplexml_load_file($url);
        //$string = file_get_contents($url);
        //echo $string;
        //$xml = simplexml_load_string($string);
        
        foreach ($xml as $value) {
            $id = $value->id;
            $nome = $value->nome;
            $partido = $value->partido;
            $localizacao = $value->tagLocalizacao;
            $deputado = new Deputado($id, $nome, $partido, $localizacao);
            DeputadoDao::insereDeputado($deputado);
            
        }
        
        return TRUE;
    }
    
    public static function gravaDespesas ($lista) {
        while($linha  = $lista->fetchArray()) {
            $id = $linha["id"];
            for ($i = 1; $i <= 12; $i++) {
                $url = "http://dadosabertos.almg.gov.br/ws/prestacao_contas/verbas_indenizatorias/deputados/$id/2014/$i";
                XmlController::leDespesas($url);
                
            }

        }
    }
    public static function leDespesas ($url) {
        
        $xml = simplexml_load_file($url);
        foreach ($xml as $value) {
            $idDeputado = $value->idDeputado;
            $data = $value->dataReferencia;
            $valor = str_replace (",", ".", $value->valor);
            $descricao = $value->descTipoDespesa;
            $codTipoDespesa = TipoDespesaDao::insereTipoDespesa($descricao);
            $despesa = new Despesa(0, $idDeputado, $codTipoDespesa, $data, $valor);
            DespesaDao::insereDespesa($despesa);
        }
    }
}
