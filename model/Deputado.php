<?php
/**
 * Description of Deputado
 *
 * @author Allan
 */
class Deputado {
    private $id;
    private $nome;
    private $partido;
    private $localizacao;
    public function __construct($id, $nome, $partido, $localizacao) {
        $this->id = $id + 0;
        $this->nome = $nome . "";
        $this->partido = $partido . "";
        $this->localizacao = $localizacao + 0;
        
        ;
    }
    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getPartido() {
        return $this->partido;
    }

    function getLocalizacao() {
        return $this->localizacao;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setPartido($partido) {
        $this->partido = $partido;
    }

    function setLocalizacao($localizacao) {
        $this->localizacao = $localizacao;
    }
    public function toString () {
        return "$this->id - $this->nome - $this->partido - $this->localizacao";
    }

}
