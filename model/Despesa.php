<?php
/**
 * Description of Despesa
 *
 * @author Allan
 */
class Despesa {
    private $codDespesa;
    private $idDeputado;
    private $codTipoDespesa;
    private $data;
    private $valor;
    function __construct($codDespesa = NULL, $idDeputado, $codTipoDespesa, $data, $valor) {
        $this->codDespesa = $codDespesa + 0;
        $this->idDeputado = $idDeputado + 0;
        $this->codTipoDespesa = $codTipoDespesa + 0;
        $this->data = $data;
        $this->valor = $valor + 0.0;
    }
    function getCodDespesa() {
        return $this->codDespesa;
    }

    function getIdDeputado() {
        return $this->idDeputado;
    }

    function getCodTipoDespesa() {
        return $this->codTipoDespesa;
    }

    function getData() {
        return $this->data;
    }

    function getValor() {
        return $this->valor;
    }

    function setCodDespesa($codDespesa) {
        $this->codDespesa = $codDespesa;
    }

    function setIdDeputado($idDeputado) {
        $this->idDeputado = $idDeputado;
    }

    function setCodTipoDespesa($codTipoDespesa) {
        $this->codTipoDespesa = $codTipoDespesa;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }



}
