<?php
/**
 * Description of TipoDespesaDao
 *
 * @author Allan
 */
require_once 'Conexao.php';
class TipoDespesaDao {
    public static function insereTipoDespesa($descricao) {
        $sql = "select cod, descricao from tipo_despesa where descricao = '$descricao'";
        $result = Conexao::executaSql($sql);
        $linha = $result->fetchArray();
        if(! isset($linha["cod"])) {
            $sql = "INSERT INTO tipo_despesa(descricao) values ('$descricao')";
            Conexao::executaSql($sql);
            $sql = "select cod, descricao from tipo_despesa where descricao = '$descricao'";
            $result = Conexao::executaSql($sql);
            $linha = $result->fetchArray();
        }
        return $linha["cod"];

    }
    
    public static function listaTipoDespesa () {
        $sql = "select cod, descricao from tipo_despesa ";
        $result = Conexao::executaSql($sql);
        return $result;
    }

}
