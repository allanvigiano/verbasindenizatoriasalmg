<?php
/**
 * Description of DespesaDao
 *
 * @author Allan
 */
require_once 'Conexao.php';
class DespesaDao {
    public static function insereDespesa($despesa) {
        $idDeputado = $despesa->getIdDeputado();
        $codTipoDespesa = $despesa->getCodTipoDespesa();
        $data = $despesa->getData();
        $valor = $despesa->getValor();
        $sql = "INSERT INTO despesa(id_deputado, cod_tipo_despesa, data, valor) values ($idDeputado, $codTipoDespesa, '$data', $valor)";
        
        Conexao::executaSql($sql);
    }
    public static function retornaGastos ($idDeputado) {
        $sql = "
                select d.id_deputado, dep.nome, d.cod_tipo_despesa, tipo.descricao, sum (d.valor)
                from despesa as d 
                    inner join deputado as dep
                                on d.id_deputado = dep.id
                    inner join tipo_despesa as tipo
                        on d.cod_tipo_despesa = tipo.cod
                where d.id_deputado = $idDeputado
                group by d.cod_tipo_despesa";
    }
    public static function totalPorCategoria() {
        $sql =
                "select tipo.descricao categoria, sum (d.valor) total
                from despesa as d
                    inner join tipo_despesa as tipo
                        on d.cod_tipo_despesa = tipo.cod
                group by tipo.descricao
                order by total desc, categoria";
        $result = Conexao::executaSql($sql);
        return $result;
    }
    public static function cincoMaioresIndenizacoes($categoria) {
        $sql =
                "select dep.nome as nome, tipo.descricao as categoria, sum (d.valor) as valor
                from despesa as d 
                    inner join deputado as dep
                                on d.id_deputado = dep.id
                    inner join tipo_despesa as tipo
                        on d.cod_tipo_despesa = tipo.cod
                where d.cod_tipo_despesa = $categoria
                group by d.id_deputado
                order by sum(d.valor) desc
                LIMIT 5;";
        $result = Conexao::executaSql($sql);
        return $result;
    }
    public static function totalOcorrenciasDespesa () {
        $sql = 
                "select count(cod_despesa) as total from despesa";
        $result = Conexao::executaSql($sql);
        $linha = $result->fetchArray();
        return $linha["total"];
    }
    public static function apagaOcorrencias () {
        $sql = 
                "delete from despesa";
        Conexao::executaSql($sql);
    }
}
