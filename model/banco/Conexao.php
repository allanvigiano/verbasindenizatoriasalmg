<?php
/**
 * Description of Conexao
 * 
 * Classe para gerenciar as conexões com o banco de dados SQLite
 *
 * @author Allan Vigiano
 */
class Conexao {
    public static $db;
    public static function criaBanco() {
        Conexao::$db = new SQLite3('banco.db',SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
    }

    static function criaTabelas () {
        /*
         * deputado(id, nome, partido, localizacao)
         * tipo_despesa(cod, desc)
         * despesa(cod_despesa, id_deputado, cod_tipo_despesa, data valor)
         */
        
        $create1 = 
                "CREATE TABLE deputado (
                    id INTEGER NOT NULL PRIMARY KEY,
                    nome VARCHAR(50) NOT NULL,
                    partido VARCHAR(10) NOT NULL,
                    localizacao INTEGER
                )";
        $create2 = 
                "CREATE TABLE tipo_despesa (
                    cod INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                    descricao VARCHAR(50) UNIQUE NOT NULL 
                )";
        $create3 = 
                "create table despesa
                (
                    cod_despesa INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    id_deputado INTEGER NOT NULL,
                    cod_tipo_despesa INTEGER NOT NULL,
                    data DATE NOT NULL,
                    valor NUMERIC(9, 2),
                    FOREIGN KEY (id_deputado) REFERENCES deputado(id),
                    FOREIGN KEY (cod_tipo_despesa) REFERENCES tipo_despesa(cod)
                )";
        Conexao::$db->query($create1); 
        Conexao::$db->query($create2);
        Conexao::$db->query($create3);
        
    }
    public static function executaSql($sql) {
        //echo $sql; exit();
        return Conexao::$db->query($sql);
        
    }
    public static function sqlite_table_exists($nomeTabela) 
    {   
        Conexao::criaBanco();
        $result = Conexao::$db->query("SELECT name FROM sqlite_master WHERE 
                                  type='table' AND name like '%$nomeTabela%'"); 
        $linha = $result->fetchArray();
        //echo "Nome tabela($nomeTabela): ".$linha["name"]; exit;
        return $linha["name"] === $nomeTabela? TRUE : FALSE; 
    } 

}
