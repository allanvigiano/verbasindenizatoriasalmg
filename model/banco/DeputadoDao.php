<?php
/**
 * Description of DeputadoDao
 *
 * @author Allan
 */
require_once 'Conexao.php';
class DeputadoDao {
    public static function insereDeputado($deputado) {
        $id = $deputado->getId();
        $nome = $deputado->getNome();
        $partido = $deputado->getPartido();
        $localizacao = $deputado->getLocalizacao();
        
        $sql = "INSERT INTO deputado(id, nome, partido, localizacao) values ($id, '$nome', '$partido', $localizacao)";
        
        Conexao::executaSql($sql);
    }
    public static function retornaTodosDeputados () {
        $sql=
                "SELECT * FROM deputado";
        return Conexao::executaSql($sql);
                
    }
    public static function retornaNumLinhas() {
        if (Conexao::sqlite_table_exists("deputado")) {
            $sql ="SELECT COUNT(id) as numLinhas from deputado";
            $result = Conexao::executaSql($sql);
            return $result->fetchArray()["numLinhas"];
        }
        else {
            return -1;
        }
    }
}
