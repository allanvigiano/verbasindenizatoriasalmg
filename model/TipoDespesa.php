<?php
/**
 * Description of TipoDespesa
 *
 * @author Allan
 */
class TipoDespesa {
    private $codTipoDespesa;
    private $descTipoDespesa;
    function __construct($codTipoDespesa = NULL, $descTipoDespesa) {
        $this->codTipoDespesa = $codTipoDespesa;
        $this->descTipoDespesa = $descTipoDespesa;
    }
    function getCodTipoDespesa() {
        return $this->codTipoDespesa;
    }

    function getDescTipoDespesa() {
        return $this->descTipoDespesa;
    }

    function setCodTipoDespesa($codTipoDespesa) {
        $this->codTipoDespesa = $codTipoDespesa;
    }

    function setDescTipoDespesa($descTipoDespesa) {
        $this->descTipoDespesa = $descTipoDespesa;
    }


}
