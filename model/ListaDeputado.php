<?php
/**
 * Description of ListaDeputado
 *
 * @author Allan
 */
require_once 'Deputado.php';
class ListaDeputado {
    private $listaDeputados;
    function __construct() {
        $this->listaDeputados = array();
    }
    public function insere ($deputado) {
        $this->listaDeputados[] = $deputado;

    }
    function getListaDeputados() {
        return $this->listaDeputados;
    }

        public function getElemento($i) {
        return $this->listaDeputados[$i];
    }
    public function toString () {
        $string = "";
        foreach ($this->listaDeputados as $key => $value) {
            $string .= "$key - " . $value->toString() . "<br>";
        }
        return $string;
    }
    
}
