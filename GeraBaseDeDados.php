<!DOCTYPE html>
<?php
require_once 'model/banco/Conexao.php';
require_once 'model/banco/DespesaDao.php';
require_once 'model/banco/DeputadoDao.php';
//Testa se tabela de deputados já foi carregada
if(DeputadoDao::retornaNumLinhas() <= 0) {
    $glyphiconTabDeputado = "glyphicon glyphicon-minus-sign";
    $disableTabDeputado = "";
    $glyphiconDespesas = "glyphicon glyphicon-minus-sign";
    $disableDespesas = "disabled";
    $outrosDados = "";
    $numOcorrencias = 0;
}
else {
    $glyphiconTabDeputado = "glyphicon glyphicon-ok-sign";
    $disableTabDeputado = "disabled";
    
    //Sobre as despesas...
    $numOcorrencias = DespesaDao::totalOcorrenciasDespesa();
    if($numOcorrencias == "0") {
        $glyphiconDespesas = "glyphicon glyphicon-minus-sign";
        $disableDespesas = "";
        $outrosDados = "disabled";
    }
    else {
        $glyphiconDespesas = "glyphicon glyphicon-ok-sign";
        $disableDespesas = "disabled";
        $outrosDados = "";
    }
    
}

?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Sistema X</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/folhaDeEstilo.css" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.3.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/GeraBaseDeDados.js"></script>
        
        <nav class="navbar navbar-static-top navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Sistema X</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Serviços <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="GeraBaseDeDados.php">Assembleia Legislativa de Minas Gerais</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="container-fluid dados">
        
            <button id="botaoDeputados" type="button" class="bottomMargin btn btn-default <?php echo $disableTabDeputado ?>" onclick="pegaDeputados()" <?php echo $disableTabDeputado ?>>
                <span class="<?php echo $glyphiconTabDeputado?>" aria-hidden="true"></span>
                Gravar Lista de Deputados no Banco de Dados
            </button>
            <br>
            <div class="bottomMargin">
                <button id="botaoDespesas" type="button" class="btn btn-default <?php echo $disableDespesas ?>" onclick="pegaDespesas()" <?php echo $disableDespesas ?>>
                    <span class="<?php echo $glyphiconDespesas ?>" aria-hidden="true"></span>
                    Gravar Lista de Despesas de 2014 (<?php echo $numOcorrencias ; ?> ocorrências já adicionadas)
                </button>
                <button id="botaoApaga" type="button" class="btn-sm btn-danger " onclick="apagaOcorrencias()">Apagar ocorrências</button>
            </div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" onclick="pegaTotalPorCategoria()">
                                Lista com os gastos totais por categoria
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div id="verbas2014"></div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" onclick="pegaCincoMaiores()">
                                Mairoes indenizações pagas por categoria
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <div id="cincoMaiores"></div>
                        </div>
                    </div>
                  </div>
            </div>
        </div>
        <div id="modalCarregando" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div id='mensagemModal' class="modal-header">
                    </div>
                    <div class="modal-body">
                        <div class="progress">
                            <div id="barraProgresso" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>