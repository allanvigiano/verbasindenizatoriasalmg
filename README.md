# Verbas Indenizatórias ALMG #
O principal motivo deste repositório é demonstrar meus conhecimentos em 
programação web para uma possível seleção de emprego.
Devo agradecer à empresa que me proporcionou este estudo, foi muito proveitoso!
(esta aplicação deve atender aos requisitos solicitados, porém continuarei
adicionando melhorias)

# Instruções de uso #
Obs: estes fontes foram criados usando:
IDE NetBeans, 
PHP 5.6.12, 
Xampp for Windows 5.6.12.

1. Copie os fontes para o seu servidor Apache
2. Abra a página em um navegador
3. No menu "Serviços",  selecione "Assembleia Legislativa de Minas Gerais"
4. Siga, sequecialmente, as opções desejadas.

**Em Linux, não se esqueça de mudar as permissões do diretório raiz (e arquivos e pastas nele contidos): habilite as 
permissões de leitura e escrita.**