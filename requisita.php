<?php
session_start(1);
set_time_limit(0); // evita que dê timeout na leitura do XML
require_once './controller/XmlController.php';
require_once './model/banco/Conexao.php';
require_once './model/ListaDeputado.php';
$acao = $_POST["acao"];

/*
 * Esta página PHP tem a finalidade de intermediar todas as requisições vindas 
 * do navegador.
 */

//Cria Banco de Dados caso ele não exista

Conexao::criaBanco();
switch ($acao) {
    case "deputados":
        $numLinhas = DeputadoDao::retornaNumLinhas();
        $url = "http://dadosabertos.almg.gov.br/ws/deputados/em_exercicio";
        if($numLinhas == -1) {
            Conexao::criaTabelas();
        }
        else if ($numLinhas > 0) {
            return;
        }
        XmlController::gravaListaDeputadosEmExercicio($url);
        
        break;
    case "despesas":
        $lista = DeputadoDao::retornaTodosDeputados();
        XmlController::gravaDespesas($lista);

        break;
    case "totalPorCategoria":
        $string = "<div class='panel panel-default'>
                        <div class='panel-heading'>
                            <h4>Verbas de Indenização pagas em 2014</h4>
                        </div>
                        <table class='table'>";
        $result = DespesaDao::totalPorCategoria();
        
        while ($linha = $result->fetchArray()) {
            $string .= "<tr><td>".$linha["categoria"] . "</td><td>" . number_format_br($linha["total"]) . "</td></tr>";
            
        }
        $string .= "</table></div>";
        //Esse 'echo' será escrito no navegador, gerando uma tabela.
        echo $string;
        break;
        
    case "cincoMaiores":
        $string = "";
       
        $resultCategoria = TipoDespesaDao::listaTipoDespesa();
        while ($linhaCategoria = $resultCategoria->fetchArraY()) {
            $grupo = 
                    "<div class='panel panel-default painelCategoria'>
                        <div class='panel-heading'>
                            <h4>" . $linhaCategoria["descricao"] . "</h4>
                        </div>
                        <table class='table'>";
            $result = DespesaDao::cincoMaioresIndenizacoes($linhaCategoria["cod"]);
            while ($linha = $result->fetchArray()) {
                $grupo .= "<tr><td>".$linha["nome"] . "</td><td>" . number_format_br($linha["valor"]) . "</td></tr>";
            }
            $grupo .= "</table></div>";
            $string .= $grupo;
        }
        
        echo $string;
        break;
    case "apagaOcorrencias":
        DespesaDao::apagaOcorrencias();
        break;
    default:
        break;
   
}
function number_format_br ($float) {
    return
            "R$" . number_format($float, 2, ',', '.');
}