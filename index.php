<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Sistema X</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        
        <!--Meu estilo-->
        <link href="css/folhaDeEstilo.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.3.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
        <nav class="navbar navbar-static-top navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Sistema X</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Serviços <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="GeraBaseDeDados.php">Assembleia Legislativa de Minas Gerais</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
        <div class="container-fluid">
            <div class="jumbotron">
                <h1>Olá, que bom que você está usando nosso sistema!</h1>
                <h3>Alguns objetivos desta aplicação: </h3>
                <p>
                    <li>Consumir os dados abertos da Assembléia Legislativa do Estado de Minas Gerais;</li>
                    <li>Armazenar os dados em um banco de dados SQLite (recomenda-se fazer um script de linha de comando que busque os dados);
                        <ul class="comentario">
                            Nesta aplicação o PHP foi utilizado para buscar os dados nos XMLs
                        </ul>
                    </li>
                    <li>Mostrar a listagem do gasto total em verbas indenizatórias por categoria no ano de 2014 ordenados do maior para o menor;</li>
                    <li>Mostrar os top 5 deputados que mais gastaram verbas em cada uma delas.</li>
                    <li>Implementar testes de unidade para o código.</li>

                </p>
            </div>
        </div>

    </body>
</html>