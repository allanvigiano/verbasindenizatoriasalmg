var flagTotalPorCategoria = false;
var flagCincoMaiores = false;
function pegaDeputados() {
    $("#mensagemModal").text("Buscando Deputados...");
    $('#modalCarregando').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
    $.post("requisita.php",
    {
        acao: "deputados",
    })
    .done(function(resposta) {
        
        if (resposta !== "") {
            alert("Houve um erro!" + resposta);
            return;
        }
        $("#botaoDeputados").addClass("disabled");
        $("#botaoDeputados").attr("disabled", "disabled");
        $("#botaoDeputados > span").removeClass("glyphicon-minus-sign");
        $("#botaoDeputados > span").addClass("glyphicon-ok-sign");
        $("#botaoDespesas").removeAttr("disabled");
        $("#botaoDespesas").removeClass("disabled");
        
        $('#modalCarregando').modal('hide');
    });
}
function pegaDespesas() {
    $("#mensagemModal").text("Buscando indenizações. Esse processo pode ser demorado, cerca de cinco minutos!");
    $('#modalCarregando').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
    $.post("requisita.php",
    {
        acao: "despesas"
    }, function () {
        
    })
    .done(function(resposta) {
        if (resposta !== "") {
            alert("Houve um erro!" + resposta);
            return;
        }
        window.location.reload();
        $('#modalCarregando').modal('hide');
    });
}
function pegaTotalPorCategoria () {
    if (flagTotalPorCategoria) {
        //dados gerados anteriormente
        return;
    }
    $('#modalCarregando').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
     $.post("requisita.php",
    {
        acao: "totalPorCategoria",
    })
    .done(function(resposta) {
        $("#verbas2014").html(resposta);
        $('#modalCarregando').modal('hide');
    });
    flagTotalPorCategoria = true;
}
function pegaCincoMaiores () {
    if (flagCincoMaiores) {
    //dados gerados anteriormente
    return;
    }
    $('#modalCarregando').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
    $.post("requisita.php",
    {
        acao: "cincoMaiores",
        categoria: 1,
    })
    .done(function(resposta) {
        $("#cincoMaiores").html(resposta);
        $('#modalCarregando').modal('hide');
    });
    flagCincoMaiores = true;
}
function apagaOcorrencias () {
    $('#modalCarregando').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });
    $.post("requisita.php",
    {
        acao: "apagaOcorrencias",
    })
    .done(function() {
        window.location.reload();

    })
    .always(function () {
        $('#modalCarregando').modal('hide');
    });
}